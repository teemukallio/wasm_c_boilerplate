#include <emscripten/emscripten.h>

extern "C" {
  int main(int argc, char ** argv) {
    printf("%s\n", argv[1]);
  }

  int EMSCRIPTEN_KEEPALIVE world() {
    printf("World\n");
    return 123;
  }
}
