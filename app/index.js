import 'styles/index.scss';

import hello from './hello.c';

hello("Hello")
  .then(module => {
    const val = module._world();
    console.log(val);
  });
